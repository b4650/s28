// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(`The item "${json.title} has a status of ${json.completed}`));

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.